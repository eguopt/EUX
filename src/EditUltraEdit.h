#ifndef _H_EDITULTRA_EDIT_
#define _H_EDITULTRA_EDIT_

#include "framework.h"

int OnUndoEdit( struct TabPage *pnodeTabPage );
int OnRedoEdit( struct TabPage *pnodeTabPage );

int OnCutText( struct TabPage *pnodeTabPage );
int OnCopyText( struct TabPage *pnodeTabPage );
int OnPasteText( struct TabPage *pnodeTabPage );
int OnDeleteText( struct TabPage *pnodeTabPage );
int OnCutLine( struct TabPage *pnodeTabPage );
int OnCutLineAndPasteLine( struct TabPage *pnodeTabPage );
int OnCopyLine( struct TabPage *pnodeTabPage );
int OnCopyLineAndPasteLine( struct TabPage *pnodeTabPage );
int OnCopyFilename( struct TabPage *pnodeTabPage );
int OnCopyPathname( struct TabPage *pnodeTabPage );
int OnCopyPathFilename( struct TabPage *pnodeTabPage );
int OnPasteLine( struct TabPage *pnodeTabPage );
int OnPasteLineUpstairs( struct TabPage *pnodeTabPage );
int OnDeleteLine( struct TabPage *pnodeTabPage );
int OnDeleteWhiteCharacterAtLineHead( struct TabPage *pnodeTabPage );
int OnDeleteWhiteCharacterAtLineTail( struct TabPage *pnodeTabPage );
int OnDeleteBlankLine( struct TabPage *pnodeTabPage );
int OnDeleteBlankLineWithWhiteCharacter( struct TabPage *pnodeTabPage );

int OnJoinLine( struct TabPage *pnodeTabPage );

int OnLowerCaseEdit( struct TabPage *pnodeTabPage );
int OnUpperCaseEdit( struct TabPage *pnodeTabPage );

int OnEditEnableAutoAddCloseChar( struct TabPage *pnodeTabPage );
int OnEditEnableAutoIdentation( struct TabPage *pnodeTabPage );

int OnEditBase64Encoding( struct TabPage *pnodeTabPage );
int OnEditBase64Decoding( struct TabPage *pnodeTabPage );
int OnEditMd5( struct TabPage *pnodeTabPage );
int OnEditSha1( struct TabPage *pnodeTabPage );
int OnEditSha256( struct TabPage *pnodeTabPage );
int OnEdit3DesCbcEncrypto( struct TabPage *pnodeTabPage );
int OnEdit3DesCbcDecrypto( struct TabPage *pnodeTabPage );

#endif
