#ifndef _H_EDITULTRA_FILETREEBAR_
#define _H_EDITULTRA_FILETREEBAR_

#include "framework.h"

extern HWND	g_hwndFileTreeBar ;
extern BOOL	g_bIsFileTreeBarShow ;
extern RECT	g_rectFileTreeBar ;

extern HWND	g_hwndFileTree ;

extern HMENU	g_hFileTreeFilePopupMenu ;
extern HMENU	g_hFileTreeDirectoryPopupMenu ;

extern int	nFileTreeImageDrive ;
extern int	nFileTreeImageOpenFold ;
extern int	nFileTreeImageClosedFold ;
extern int	nFileTreeImageTextFile ;
extern int	nFileTreeImageGeneralFile ;
extern int	nFileTreeImageExecFile ;

struct TreeViewData
{
	struct RemoteFileServer	*pstRemoteFileServer ;
	char			acPathName[ MAX_PATH ] ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acFilename[ MAX_PATH ] ;
	HTREEITEM		hti ;
	int			nImageIndex ;
	BOOL			bIsLoadedCompleted ;
};

int CreateFileTreeBar( HWND hWnd );
void AdjustFileTreeBox( RECT *rectFileTreeBar , RECT *rectFileTree );

struct TreeViewData *GetTreeViewDataFromHTREEITEM( HTREEITEM hti );
struct TreeViewData *AddFileTreeNode( HWND hwnd , HTREEITEM parent , int nImageIndex , struct RemoteFileServer *pstRemoteFileServer , char *acPathName , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted );

int AppendFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent );
int LoadDrivesToFileTree( HWND hwnd );
int LoadDirectoryToFileTree( HWND hwnd , char *pcPathname );

int AppendRemoteFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent , CURL *curl );
int LoadRemoteDriverToFileTree( HWND hwnd , struct RemoteFileServer *pnodeRemoteFileServer );
int LoadRemoteDriversToFileTree( HWND hwnd );

int UpdateFileTreeNodeChildren( HWND hwnd , HTREEITEM htiParent );

int OnFileTreeNodeExpanding( NMTREEVIEW *lpnmtv );
int OnFileTreeNodeDbClick();

int RefreshFileTree();
int FileTreeRenameDirectory();
int FileTreeDeleteDirectory();
int FileTreeCreateSubDirectory();
int FileTreeCreateFile();
int FileTreeCopyFile();
int FileTreeRenameFile();
int FileTreeDeleteFile();

int LocateDirectory( char *acPathName );

#define CONFIG_KEY_MATERIAL_FILETREE		"FILETR"

#define REMOTE_FILE_BUFFER_SIZE_DEFAULT		1024*1024

struct RemoteFileBuffer
{
	char	buf[ REMOTE_FILE_BUFFER_SIZE_DEFAULT ] ;
	size_t	str_len ;
	size_t	remain_len ;
};

#endif
